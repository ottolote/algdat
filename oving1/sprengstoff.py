from sys import stdin

class Kubbe:
    vekt = None
    neste = None
    def __init__(self, vekt):
        self.vekt = vekt 

def spor(kubbe):
    current_kubbe = kubbe
    max_vekt = current_kubbe.vekt
    while current_kubbe.neste:
        current_kubbe = current_kubbe.neste
        if current_kubbe.vekt > max_vekt:
            max_vekt = current_kubbe.vekt
    
    if current_kubbe.vekt > max_vekt:
        max_vekt = current_kubbe.vekt
    return max_vekt






# Oppretter lenket liste
forste = None
siste = None
for linje in stdin:
    #print "Linje %s" % linje,
    forrige_siste = siste
    siste = Kubbe(int(linje))
    if forste == None:
	forste = siste
    else:
	forrige_siste.neste = siste

# Kaller loesningsfunksjonen og skriver ut resultatet
print spor(forste)
