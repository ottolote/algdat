def finddraw(x):      #x>1
    return (x-1)%7

def printaction(n):
    draw = finddraw(n)
    if draw: #finddraw(i) != 0
        print "%d: draw %d, %d remaining after draw" % (n, draw, n-draw)
    else:
        print "%d: give up" % n

for i in xrange(1,30):
    printaction(i)

printaction(246)
printaction(250)

