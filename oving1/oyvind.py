from sys import stdin

# Lager elementer
class Kubbe:
    vekt = None
    neste = None

    def __init__(self, vekt):
        self.vekt = vekt 
        self.neste = None 

#Returnerer hoyeste verdi i en liste
def spor(kubbe):
	storsteKubbe = kubbe.vekt 
	temp = kubbe
	
	while temp.neste != None:
		if temp.vekt > storsteKubbe:	
			storsteKubbe = temp.vekt
		
		temp = temp.neste
	return storsteKubbe

# Oppretter lenket liste
forste = None
siste = None
for linje in stdin:
    forrige_siste = siste
    siste = Kubbe(int(linje))
    if forste == None:
        forste = siste
    else:
        forrige_siste.neste = siste

# Kaller loesningsfunksjonen og skriver ut resultatet
print spor(forste)
