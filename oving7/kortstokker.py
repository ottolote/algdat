from sys import stdin
from itertools import repeat

def merge(decks):
    if len(decks) < 2:
        return decks

    l1 = decks[:(len(decks)/2)]
    l2 = decks[(len(decks)/2):]

    l1 = merge(l1)
    l2 = merge(l2)

    merged = []


    while l1 and l2:
        if l1[0][0] < l2[0][0]:
            merged.append(l1.pop(0))
        else:
            merged.append(l2.pop(0))
    while l1:
        merged.append(l1.pop(0))
    while l2:
        merged.append(l2.pop(0))

    return merged

            



decks = []
for line in stdin:
    (index, list) = line.split(':')
    decks += zip(map(int, list.split(',')), repeat(index))
a = ''
for tuppel in merge(decks):
    a += tuppel[1]
print a

