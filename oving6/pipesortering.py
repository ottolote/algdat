from sys import stdin
import random

def sorter(A): #quicksort test
    if len(A)<2: 
	return A
    pivot_element = random.choice(A) #using random pivots
    small = [i for i in A if i< pivot_element] #list comprehensions, might be slow
    medium = [i for i in A if i==pivot_element]
    large = [i for i in A if i> pivot_element]
    return sorter(small) + medium + sorter(large)



#slow cryptic PoS, but hey, it works
def finn(A, nedre, ovre):
    smallest = A[0]
    largest = A[0]
    if nedre in A: #O(t) = n
        smallest = nedre
    else:
        for element in A:
            if element > nedre:
                break
            smallest = element
    if ovre in A:  #O(t) = n
        largest = ovre
    else:
        for element in A:
            largest = element
            if element > ovre:
                break
    return (smallest, largest)



liste = []
for x in stdin.readline().split():
    liste.append(int(x))

sortert = sorter(liste)

for linje in stdin:
    ord = linje.split()
    minst = int(ord[0])
    maks = int(ord[1])
    resultat = finn(sortert, minst, maks)
    print str(resultat[0]) + " " + str(resultat[1])
