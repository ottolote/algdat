from sys import stdin

class Node:
    barn = None 
    ratatosk = None
    nesteBarn = None # bare til bruk i DFS
    def __init__(self):
        self.barn = []
        self.ratatosk = False
        self.nesteBarn = 0


def dfs(rot):
    stack = [ (rot) ]
    while stack:
        this = stack[stack.__len__()-1] 
        if this.ratatosk:
            return stack.__len__()-1 #0-indekserte niva
        if this.nesteBarn == this.barn.__len__():
            stack.pop() 
        else:
            stack.append(this.barn[this.nesteBarn])
            this.nesteBarn += 1



def bfs(rot):
    queue = [ (rot, 0) ] # tuple in queue: (node, depth)
    while queue:
        this, depth = queue.pop(0) 
        if this.ratatosk:
            return depth
        for barn in this.barn:
            queue.append( (barn, depth+1) )




#leser linjer i stdin
funksjon = stdin.readline().strip()
antall_noder = int(stdin.readline())

#generer array med noder
noder = []
for i in xrange(antall_noder):
    noder.append(Node())

#setter startnode og markerer ratatosk node
start_node = noder[int(stdin.readline())]
noder[int(stdin.readline())].ratatosk = True

#genererer nodetre
for line in stdin:
    tall = line.split()
    for child in xrange(1,len(tall)):
        noder[int(tall[0])].barn.append(noder[int(tall[child])])


if funksjon == 'dfs':
    print dfs(start_node)
elif funksjon == 'bfs':
    print bfs(start_node)
elif funksjon == 'velg':
    print bfs(start_node)
