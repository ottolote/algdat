from sys import stdin

def dfs(rot):
    if rot == ratatosk_node:
        return 0
    stack = [ (rot) ]
    while stack:
        this = stack[stack.__len__()-1] 
        if nodes[this][-1] == len(nodes[this])-1:
            stack.pop() 
        else:
            if nodes[this][nodes[this][-1]] == ratatosk_node:
                return len(stack)
            if nodes[this][nodes[this][-1]] in nodes:
                stack.append(nodes[this][nodes[this][-1]])
            nodes[this][-1] += 1

def bfs(rot):
    queue = [ (rot, 0) ] 
    while queue:
        this, depth = queue.pop(0) 

        if this == ratatosk_node:
            return depth
        try:
            for barn in nodes[this]:
                queue.append( (barn, depth+1) )
        except KeyError:
            continue

funksjon = stdin.readline().strip()
antall_noder = int(stdin.readline())
start_node = int(stdin.readline())
ratatosk_node = int(stdin.readline())


if not funksjon == 'dfs':
    nodes = { int(line.split(" ",1)[0]): tuple( map(int, line.split(" ")[1:])) for line in stdin.readlines() }
    print bfs(start_node)
else:
    nodes = { int(line.split(" ",1)[0]): ( map(int, line.split(" ")[1:]) + [(0)])  for line in stdin.readlines() }
    print dfs(start_node)


