from sys import stdin
from collections import deque


#using dict
def dfs(rot):
    if rot == ratatosk_node:
        return 0
    stack = deque( [rot] )
    while stack:
        this = stack[stack.__len__()-1] 
        if nodes[this][-1] == len(nodes[this])-1:
            stack.pop() 
        else:
            if nodes[this][nodes[this][-1]] == ratatosk_node:
                return stack.__len__()
            if nodes[this][nodes[this][-1]] in nodes:
                stack.append(nodes[this][nodes[this][-1]])
            nodes[this][-1] += 1


#using dict
def bfs(rot):
    queue = deque( ((rot, 0) , (1,3)  )) # tuple in queue: (node, depth)
    print queue
    while queue:
        this, depth = queue.popleft() 

        if this == ratatosk_node:
            return depth
        if this in nodes: # if this node has children
            for barn in nodes[this]:
                queue.append( (barn, depth+1) )



#leser linjer i stdin
funksjon = stdin.readline().strip()
antall_noder = int(stdin.readline())
start_node = int(stdin.readline())
ratatosk_node = int(stdin.readline())

#generate dictionary
#if funksjon == 'bfs':
if not funksjon == 'dfs':
    nodes = { int(line.split(" ",1)[0]): tuple( map(int, line.split(" ")[1:])) for line in stdin.readlines() }
    _print = bfs(start_node)
else:
    nodes = { int(line.split(" ",1)[0]): ( map(int, line.split(" ")[1:]) + [(0)])  for line in stdin.readlines() }
    _print = dfs(start_node)

print _print

#if funksjon == 'dfs':
#    print dfs(start_node)
#elif funksjon == 'bfs':
#    print bfs(start_node)
#elif funksjon == 'velg':
#    print bfs(start_node)
