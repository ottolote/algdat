#MASTER BRANCH
from sys import stdin, stderr
import traceback

class Node:
    def __init__(self):
        self.children = {}
        self.posi = []


def build(wordlist):
    topnode = Node()
    for word,pos in wordlist:
        this = topnode
        for letter in word:
            if not letter in this.children:
                this.children[letter] = Node()
            this = this.children[letter]
        this.posi.append(pos)
    return topnode


def positions(word, index, node):
    if index == len(word):
        return node.posi

    if word[index] in node.children:
        return positions(word, index+1, node.children[word[index]])

    else:
        if word[index] == '?':
            r = []
            for letter in node.children:
                #word = word[:index] + letter + word[index+1:]
                r += (positions(word, index+1, node.children[letter]))
            return r
        return []


try:
    wordlist = []
    pos = 0
    for word in stdin.readline().split():
        wordlist.append( (word,pos) )
        pos += len(word) + 1
    topnode = build(wordlist)
    for searchword in stdin:
        searchword = searchword.strip()
        print searchword + ":",
        posi = positions(searchword, 0, topnode)
        posi.sort()
        for p in posi:
            print p,
        print

except:
    traceback.print_exc(file=stderr)
